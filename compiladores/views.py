import StringIO
import json
import tempfile
import textwrap
import pydot
from pyramid.httpexceptions import HTTPExpectationFailed, HTTPNotFound
from pyramid.response import Response
from pyramid.view import view_config

import transaction
from compiladores import regex
from compiladores.models import Lang, Rgx
from compiladores.regex import write_afd, regex_valid, run_afd
from compiladores.regex.helpers import get_bigger_consecutive_seq

from .models import (
    DBSession,
    )

'''
    *************************************************************************************
    ******************************                          *****************************
    *************************************************************************************
'''


@view_config(route_name='home', renderer='json')
def my_view(request):
    pass


'''
    *************************************************************************************
    ******************************                          *****************************
    *************************************************************************************
'''


@view_config(route_name='get_langs', renderer='json')
def list_langs(request):
    return Lang.query.all()


'''
    *************************************************************************************
    ******************************                          *****************************
    *************************************************************************************
'''


@view_config(route_name='get_terms', renderer='json')
def list_terms(request):
    return [] if 'id' not in request.params else [{'t': t} for t in
                                                  sorted(list(Lang.query.get(request.params['id']).terminos))]


'''
    *************************************************************************************
    ******************************                          *****************************
    *************************************************************************************
'''


@view_config(route_name='del_lang', renderer='json')
def del_lang(request):
    try:
        if 'id' not in request.params:
            raise Exception('parametros insuficientes')

        lang = Lang.query.get(request.params['id'])

        if len(lang.rgxs) > 0:
            raise Exception('Existen regexs que dependen de este alfabeto')

        with transaction.manager:
            DBSession.delete(lang)
        return {'success': True, 'msg': 'Se elimino #%s# con exito' % lang.nombre}
    except Exception as e:
        DBSession.rollback()
        return {'success': False, 'msg': e.message}


'''
    *************************************************************************************
    ******************************                          *****************************
    *************************************************************************************
'''


@view_config(route_name='save_lang', renderer='json')
def save_lang(request):
    try:
        if not {'id', 'nom', 'terms'}.issubset(request.params.keys()):
            raise Exception('parametros insuficientes')

        id_ = request.params['id']
        nom = request.params['nom']
        terms = set(json.loads(request.params['terms']))

        if nom == '':
            raise Exception('Nombre de alfabeto vacio, no se puede guardar')

        if len(terms) == 0:
            raise Exception('Alfabeto vacio, no se puede guardar')

        if id_ == '':
            lang = Lang()
            accion = 'agrego'
        else:
            lang = Lang.query.get(request.params['id'])
            accion = 'modifico'

        with transaction.manager:
            lang.nombre = nom
            lang.terminos = terms
            DBSession.add(lang)

        return {'success': True, 'msg': 'Se %s #%s# con exito' % (accion, nom)}
    except Exception as e:
        DBSession.rollback()
        return {'success': False, 'msg': e.message}


'''
    *************************************************************************************
    ******************************                          *****************************
    *************************************************************************************
'''


@view_config(route_name='get_rgxs', renderer='json')
def list_rgxss(request):
    return Rgx.query.all()


'''
    *************************************************************************************
    ******************************                          *****************************
    *************************************************************************************
'''


@view_config(route_name='get_langs_rgx', renderer='json')
def list_langs_of_rgx(request):
    if not {'id'}.issubset(request.params.keys()):
        return []
    return Rgx.query.get(request.params['id']).langs


'''
    *************************************************************************************
    ******************************                          *****************************
    *************************************************************************************
'''


@view_config(route_name='get_symbs_rgx', renderer='json')
def list_symbs_of_rgx(request):
    if not {'id'}.issubset(request.params.keys()):
        return []
    return [{'t': s} for s in sorted(Rgx.query.get(request.params['id']).simbols)]


'''
    *************************************************************************************
    ******************************                          *****************************
    *************************************************************************************
'''


@view_config(route_name='del_rgx', renderer='json')
def del_rgx(request):
    try:
        if 'id' not in request.params:
            raise Exception('parametros insuficientes')

        rgx = Rgx.query.get(request.params['id'])

        with transaction.manager:
            DBSession.delete(rgx)
        return {'success': True, 'msg': 'Se elimino #%s# con exito' % rgx.nombre}
    except Exception as e:
        DBSession.rollback()
        return {'success': False, 'msg': e.message}


'''
    *************************************************************************************
    ******************************                          *****************************
    *************************************************************************************
'''


@view_config(route_name='save_rgx', renderer='json')
def save_rgx(request):
    try:
        if not {'id', 'nom', 'expr', 'symbs', 'langs'}.issubset(request.params.keys()):
            raise Exception('parametros insuficientes')

        with transaction.manager:
            id_ = request.params['id']
            nom = request.params['nom']
            expr = request.params['expr']
            symbs = set(json.loads(request.params['symbs']))
            langs = DBSession.query(Lang).filter(Lang.id.in_(json.loads(request.params['langs']))).all()

            if nom == '':
                raise Exception('Nombre de regex vacio, no se puede guardar')

            if len(symbs) == 0 and len(langs) == 0:
                raise Exception('No existen posibles simbolos para la regex')

            if not regex_valid(expr):
                raise Exception('Existen caracteres invalidos en la regex')

            if not all(map(lambda c: c.isalnum(), symbs)):
                raise Exception('Los simbolos extras deben ser todos alfanumericos')

            regex.compilar_regex(expr, {l.nombre: l.terminos for l in langs}, symbs)

            #if len(reduce(lambda s,a: s & a, [l.terminos for l in langs], set())) > 0:
            #    raise Exception('Los alfabetos deben ser disjuntos')

            if id_ == '':
                rgx = Rgx()
                accion = 'agrego'
            else:
                rgx = DBSession.query(Rgx).get(request.params['id'])
                accion = 'modifico'

            rgx.nombre = nom
            rgx.expresion = expr
            rgx.simbols = symbs
            rgx.langs = langs
            if id_ == '':
                DBSession.add(rgx)

        return {'success': True, 'msg': 'Se %s #%s# con exito' % (accion, nom)}
    except Exception as e:
        DBSession.rollback()
        return {'success': False, 'msg': e.message}


'''
    *************************************************************************************
    ******************************                          *****************************
    *************************************************************************************
'''


@view_config(route_name='run_rgx', renderer='json')
def run_rgx(request):
    try:
        if not {'id', 'input'}.issubset(request.params.keys()):
            return HTTPExpectationFailed('Parametros Insuficientes')

        rgx = Rgx.query.get(request.params['id'])
        langs = {l.nombre: l.terminos for l in rgx.langs}

        afd = regex.compilar_regex(rgx.expresion, langs, rgx.simbols)

        strIO = StringIO.StringIO()

        run_afd(afd, request.params['input'], strIO)

        return {'success': True, 'msg': 'Resultado:<br>%s' % (strIO.getvalue().replace('\n', '<br>'))}
    except Exception as e:
        DBSession.rollback()
        return {'success': False, 'msg': e.message}
    strIO.seek(0)


'''
    *************************************************************************************
    ******************************                          *****************************
    *************************************************************************************
'''


@view_config(route_name='exportar_rgx')
def exportar_rgx(request):
    if not {'id'}.issubset(request.params.keys()):
        return HTTPExpectationFailed('Parametros Insuficientes')

    rgx = Rgx.query.get(request.params['id'])
    langs = {l.nombre: l.terminos for l in rgx.langs}

    afd = regex.compilar_regex(rgx.expresion, langs, rgx.simbols)
    strIO = StringIO.StringIO()
    if not write_afd(afd, strIO):
        return HTTPNotFound()

    strIO.seek(0)

    if 'view' in request.params and request.params['view'] == 'true':
        return Response(content_type='text/plain',
                        body_file=strIO,
                        content_disposition='filename="%s.py"' % rgx.nombre)
    else:
        return Response(content_type='application/x-python',
                        body_file=strIO,
                        content_disposition='attachment; filename="%s"' % rgx.nombre)


colores = {'start': 'white',
           'end': 'green',
           'error': 'red',
           'normal': 'orange'}


def elegir_color(nodo, errores, inicio, fin):
    if nodo in fin:
        return colores['end']
    elif nodo == inicio:
        return colores['start']
    else:
        if nodo in errores:
            return colores['error']
        return colores['normal']


'''
    *************************************************************************************
    ******************************                          *****************************
    *************************************************************************************
'''


@view_config(route_name='graficar_rgx')
def graficar_rgx(request):
    if not {'id'}.issubset(request.params.keys()):
        return HTTPExpectationFailed('Parametros Insuficientes')

    rgx = Rgx.query.get(request.params['id'])
    langs = {l.nombre: l.terminos for l in rgx.langs}

    title = 'AFD para \'%s\' -> %s' % (rgx.nombre, rgx.expresion)

    afd = regex.compilar_regex(rgx.expresion, langs, rgx.simbols)
    aristas, inicio, fin, symbols = afd
    nodos = set(aristas.values()) | {o for (o, s) in aristas}

    aristas_min = {}
    error = set()

    graph = pydot.Dot(graph_type='digraph', label=title)

    for nodoO in nodos:
        se_sale = False
        for s in symbols:
            key = (nodoO, aristas[(nodoO, s)])
            if aristas[(nodoO, s)] != nodoO:
                se_sale = True

            if key not in aristas_min:
                aristas_min[key] = set()
            aristas_min[key].add(s)
        if not se_sale:
            error.add(nodoO)

        nn = pydot.Node(str(nodoO), style="filled", fillcolor=elegir_color(nodoO, error, inicio, fin))
        graph.add_node(nn)

    langs_s = [(len(ts), nom, ts) for (nom, ts) in DBSession.query(Lang.nombre, Lang.terminos)]
    langs_s = sorted(langs_s)[::-1]

    for s, d in aristas_min:
        ss = aristas_min[(s, d)]
        res = ''
        longest_name_len = -1
        for ln, nom, ts in langs_s:
            if ts.issubset(ss):
                res += (', ' if res != '' else '') + nom
                ss -= ts
                longest_name_len = max(longest_name_len, len(nom))
        alnum = sorted([c for c in ss if c.isalnum()])
        ss = {c for c in ss if not c.isalnum()}

        new = get_bigger_consecutive_seq(alnum)
        while len(new) > 2:
            longest_name_len = max(longest_name_len, 3)
            res += '%s%s-%s' % (', ' if res != '' else '', new[0], new[len(new) - 1])
            alnum = sorted(set(alnum) - set(new))
            new = get_bigger_consecutive_seq(alnum)
        ss |= set(alnum)

        label = reduce(lambda s, sa: s + ', ' + sa, sorted(ss), res)
        label = label if label[0] != ',' else label[2:]

        longest_name_len = 20 if longest_name_len == -1 else longest_name_len
        label = '\n'.join(textwrap.wrap(label, longest_name_len + 3))

        edge = pydot.Edge(str(s), str(d), label=label)
        graph.add_edge(edge)

    # ok, we are set, let's save our graph into a file
    _, path = tempfile.mkstemp()
    graph.write_png(path)
    return Response(content_type='image/png',
                    body_file=open(path, 'r'),
                    content_disposition='filename="%s.png"' % rgx.nombre)
