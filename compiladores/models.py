from sqlalchemy import (
    Column,
    Integer,
    )

from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    relationship)
from sqlalchemy.schema import Table, ForeignKey
from sqlalchemy.types import PickleType, String

from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()
Base.query = DBSession.query_property()

LangRgx = Table('lang_rgx', Base.metadata,
                Column('idlang', Integer, ForeignKey('lang.id'), primary_key=True),
                Column('idrgx', Integer, ForeignKey('rgx.id'), primary_key=True)
)


class Lang(Base):
    __tablename__ = 'lang'
    id = Column(Integer, primary_key=True)
    nombre = Column(String, unique=True)
    terminos = Column(PickleType)
    rgxs = relationship('Rgx', secondary=LangRgx)

    def __init__(self, nombre='', terminos=set()):
        self.nombre = nombre
        self.terminos = terminos

    def __json__(self, *args, **kwargs):
        return {'id': self.id,
                'nombre': self.nombre,
                'terminos': reduce((lambda s, a: s + ', ' + a), sorted(self.terminos))
        }


class Rgx(Base):
    __tablename__ = 'rgx'
    id = Column(Integer, primary_key=True)
    nombre = Column(String, unique=True)
    expresion = Column(String)
    langs = relationship('Lang', secondary=LangRgx)
    simbols = Column(PickleType)

    def __init__(self, nombre='', expresion='', langs=[], simbols=set()):
        self.nombre = nombre
        self.expresion = expresion
        self.langs = langs
        self.simbols = simbols

    def __json__(self, *args, **kwargs):
        return {'id': self.id,
                'nombre': self.nombre,
                'expresion': self.expresion,
                'simbolos': reduce((lambda s, a: s + ', ' + a), sorted(self.simbols)) if len(self.simbols) > 0 else '',
                'alfabetos': reduce((lambda s, a: s + ', ' + a), sorted([l.nombre for l in self.langs])) if len(
                    self.langs) > 0 else ''
        }

#Index('my_index', MyModel.name, unique=True, mysql_length=255)
