from pyramid.config import Configurator
from sqlalchemy import engine_from_config

from .models import (
    DBSession,
    Base,
    )


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    config = Configurator(settings=settings)

    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('get_langs', '/getlangs')
    config.add_route('get_terms', '/getterms')
    config.add_route('del_lang', '/dellang')
    config.add_route('save_lang', '/savelang')

    config.add_route('get_rgxs', '/getrgxs')
    config.add_route('get_langs_rgx', '/getlangsrgx')
    config.add_route('get_symbs_rgx', '/getsymbsrgx')
    config.add_route('del_rgx', '/delrgx')
    config.add_route('save_rgx', '/savergx')

    config.add_route('run_rgx', '/runrgx')
    config.add_route('graficar_rgx', '/graficar')
    config.add_route('exportar_rgx', '/exportar')

    config.scan()
    return config.make_wsgi_app()
