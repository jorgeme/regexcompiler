import Queue
import itertools
from analex_basics import Analex
from thompson_ops import simple_block, concat_blocks, kleen_lock, pos_kleen_lock, optional_block, or_blocks, lang_block

__author__ = 'jorge'


class Thompson:
    def __init__(self, regex, lenguajes, simbolos):
        self.analex = Analex(regex)
        self.langs = lenguajes
        self.symbols = simbolos

    def __id___(self):
        s = ''
        while not self.analex.empty() and self.analex.peek().isalnum():
            s += self.analex.pop()
        if s in self.langs:
            return lang_block(self.langs[s])
        elif s in self.symbols:
            return simple_block(s)

        raise Exception('Lenguaje o simbolo desconocido "%s"' % s)

    def __concat_r__(self, a, r_res):
        if r_res is None:
            return a

        if r_res['op'] == '.':
            re = concat_blocks(a, r_res['t'])
        elif r_res['op'] == '|':
            re = or_blocks(a, r_res['t'])
        elif r_res['op'] == '*':
            re = kleen_lock(a)
        elif r_res['op'] == '+':
            re = pos_kleen_lock(a)
        elif r_res['op'] == '?':
            re = optional_block(a)
        else:
            re = a
        return re if r_res['r'] is None else self.__concat_r__(re, r_res['r'])

    def __r___(self):
        r = {'op': None, 'r': None}
        if self.analex.peek() in list('*?+'):
            r['op'] = self.analex.pop()
            r['r'] = self.__r___()
        elif self.analex.peek() in list('.|'):
            r['op'] = self.analex.pop()
            r['t'] = self.__termino__()
            r['r'] = self.__r___()
        elif self.analex.empty():
            return None
        return r

    def __termino__(self):
        if self.analex.peek().isalnum():
            pri = self.__id___()
        elif self.analex.peek() == '(':
            self.analex.match('(')
            pri = self.__termino__()
            self.analex.match(')')
        return self.__concat_r__(pri, self.__r___())

    def __call__(self, *args, **kwargs):
        afn = self.__termino__()
        if not self.analex.empty():
            raise Exception('Caracter inesperado: "%s"' % self.analex.peek())

        return self.get_aristas(afn)

    def get_aristas(self, afd):
        ns = {}
        ngen = itertools.count()
        aristas = set()

        bfsq = Queue.Queue()
        bfsq.put(afd[0])
        while not bfsq.empty():
            nodo = bfsq.get()

            if nodo in ns:
                continue

            ns[nodo] = ngen.next()
            for next in nodo.nexts:
                if next not in ns:
                    bfsq.put(next)

        def recur(nodo):
            for next in nodo.nexts:
                arista = (ns[nodo], ns[next], nodo.val)
                if arista not in aristas:
                    aristas.add(arista)
                    recur(next)

        recur(afd[0])
        return aristas, ns[afd[0]], ns[afd[1]]