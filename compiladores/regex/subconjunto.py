import Queue

__author__ = 'jorge'


class Subconjunto:
    def __init__(self, inicio, aristas, fin, simbolos):
        self.aristas = aristas
        self.inicio = inicio
        self.fin = fin
        self.simbolos = simbolos

    def mover(self, frm, sim):
        return {destino for origen, destino, symbol in self.aristas if origen in frm and sim == symbol}

    def epsilon_lock_individual(self, t, visited):
        if t in visited:
            return set()
        ars = self.mover([t], None)
        #{destino for origen, destino, symbol in self.aristas if t == origen and symbol is None}
        return {t} | ars | self.epsilon_lock(ars, visited | {t})

    def epsilon_lock(self, T, visited=set()):
        try:
            return reduce(lambda sm, t: sm | self.epsilon_lock_individual(t, visited), T, set())
        except TypeError:
            return self.epsilon_lock_individual(T, visited)

    def __call__(self, *args, **kwargs):
        inicial = self.epsilon_lock(self.inicio)
        estados = [set(), inicial]
        aristas = {(0, s): 0 for s in self.simbolos}
        q = Queue.Queue()
        q.put(inicial)
        while not q.empty():
            e_actual = q.get()
            idx_actual = estados.index(e_actual)
            for sym in self.simbolos:
                new = self.epsilon_lock(self.mover(e_actual, sym))
                if new not in estados:
                    estados.append(new)
                    q.put(new)
                aristas[(idx_actual, sym)] = estados.index(new)
        return aristas, 1, [i for i, estado in enumerate(estados) if self.fin in estado]



