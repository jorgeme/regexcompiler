#!/usr/bin/env python
import base64
import pickle
import sys

__author__ = 'jorge'

if __name__ == "__main__":
    transiciones, inicio, finales, simbolos = pickle.loads(base64.b64decode("#afd"))
    verbose = False

    def run_afd(input_):
        ginput = input_
        estado = inicio
        processed = ''
        maxpartial = ''
        while input_ != '':
            if input_[0] not in simbolos:
                if verbose:
                    print 'Simbolo no reconocido <<%s>> en \'%s\'' % (input_[0], ginput)
                estado = None
                break
            estado = transiciones[(estado, input_[0])]
            processed += input_[0]
            if estado in finales:
                maxpartial = processed
            input_ = input_[1:]

        if verbose:
            print 'Maximo match \'%s\' de \'%s\'' % (maxpartial, ginput)

        return estado in finales

    sys.argv = sys.argv[1:]
    if sys.argv[0] == '-v':
        verbose = True
        sys.argv = sys.argv[1:]

    for expr in sys.argv:
        print run_afd(expr)