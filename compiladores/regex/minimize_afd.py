__author__ = 'jorge'


class MinimizeAfd:
    def __init__(self, aristas, inicio, finales, simbolos):
        self.aristas = aristas
        self.inicio = inicio
        self.finales = set(finales)
        self.simbolos = simbolos

    def idx_in(self, space, element):
        return [i for i, e in enumerate(space) if element in e][0]

    def transiciones_estado(self, pi, estado):
        return [self.idx_in(pi, self.aristas[(estado, s)]) for s in self.simbolos]

    def __call__(self):
        estados = {o for o, s in self.aristas} | {self.aristas[(o, s)] for o, s in self.aristas}
        pi_new, pi = [{e for e in estados if e not in self.finales}, {e for e in estados if e in self.finales}], []
        pi_new = [cs for cs in pi_new if cs != set()]
        while pi != pi_new:
            pi, pi_new = pi_new, []
            for conjunto in pi:
                if len(conjunto) == 1:
                    pi_new.append(conjunto)
                else:
                    trans0 = self.transiciones_estado(pi, set(conjunto).pop())
                    a, b = set(), set()
                    for estado in conjunto:
                        (a if self.transiciones_estado(pi, estado) == trans0 else b).add(estado)
                    pi_new.append(a)
                    if b != set():
                        pi_new.append(b)
        aristas = {}
        inicio = None
        finales = set()
        for idx_actual, conjunto in enumerate(pi):
            aristas.update({(idx_actual, sym): dest for sym, dest in
                            zip(self.simbolos, self.transiciones_estado(pi, set(conjunto).pop()))})
            if self.inicio in conjunto:
                inicio = idx_actual
            if self.finales.intersection(conjunto) != set():
                finales.add(idx_actual)

        estados = range(len(pi))

        aristas_new, aristas = aristas, {}
        #print 'antes %r' % aristas_new
        while aristas != aristas_new:
            aristas = aristas_new
            for estado in estados:
                if estado == inicio:
                    continue
                not_with_estado = {(o, s): d for (o, s), d in aristas_new.iteritems() if o != estado}
                aristas_new = aristas_new if estado in not_with_estado.values() else not_with_estado

        estados = {o for o, s in aristas} | {d for (o, s), d in aristas.iteritems()}

        finales = finales.intersection(estados)

        return aristas, inicio, list(finales)

