__author__ = 'jorge'


class Arista(object):
    def __init__(self, val=None, nexts=[]):
        self.val = val
        self.nexts = list(nexts)


def lang_block(lang):
    exit0 = Arista()
    begin0 = Arista(nexts=[Arista(val=s, nexts=[exit0]) for s in lang])
    return begin0, exit0


def simple_block(val):
    exit0 = Arista()
    b = Arista(val=val, nexts=[exit0])
    return b, exit0


def concat_blocks(b1, b2):
    b1[1].nexts.append(b2[0])
    return b1[0], b2[1]


def or_blocks(b1, b2):
    begin0 = Arista(nexts=[b1[0], b2[0]])
    exit0 = Arista()
    b1[1].nexts.append(exit0)
    b2[1].nexts.append(exit0)
    return begin0, exit0


def optional_block(b):
    exit0 = Arista()
    begin0 = Arista(nexts=[b[0], exit0])
    b[1].nexts.append(exit0)
    return begin0, exit0


def pos_kleen_lock(b):
    exit0 = Arista()
    #begin0 = Arista(nexts=[b[0]])
    b[1].nexts.append(exit0)
    b[1].nexts.append(b[0])
    return b[0], exit0


def kleen_lock(b):
    return optional_block(pos_kleen_lock(b))

