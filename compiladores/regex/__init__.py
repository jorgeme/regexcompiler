import base64
import pickle
from compiladores.regex.minimize_afd import MinimizeAfd
from compiladores.regex.subconjunto import Subconjunto
from compiladores.regex.thompson import Thompson

__author__ = 'jorge'


def compilar_regex(string, langs, symbols):
    if not regex_valid(string):
        raise Exception('La expresion regular contiene caracteres invalidos')
    simbolos = reduce((lambda s, a: s | a), langs.values(), symbols)
    string = limpiar_regex(string)
    aristas, i, f = Thompson(string, langs, simbolos)()
    print aristas
    #simbolos = {s for o, d, s in aristas if s is not None}
    aristas, i, fs = Subconjunto(i, aristas, f, simbolos)()
    aristas, i, fs = MinimizeAfd(aristas, i, fs, simbolos)()
    aristas, i, fs = MinimizeAfd(aristas, i, fs, simbolos)()
    return aristas, i, fs, simbolos


def write_afd(afd, out_file):
    afd = base64.b64encode(pickle.dumps(afd))
    try:
        with open('compiladores/regex/afd_template.py', 'r') as template:
            for line in template:
                line = line if '#afd' not in line else line.replace('#afd', afd)
                out_file.write(line)
    except IOError:
        return False
    return True


def limpiar_doubles(rgx, ch):
    old, new = '', rgx
    while old != new:
        old = new
        new = new.replace(ch * 2, ch)
    return new


def limpiar_regex(rgx):
    rgx = rgx.replace(' ', '')
    rgx = limpiar_doubles(rgx, '*')
    rgx = limpiar_doubles(rgx, '?')
    rgx = limpiar_doubles(rgx, '+')
    return rgx


def regex_valid(rgx):
    return all([c.isalnum() or c in '.|()?+*' for c in rgx])


def run_afd(afd, input_, out, verbose=True):
    transiciones, inicio, finales, simbolos = afd
    ginput = input_
    estado = inicio
    processed = ''
    maxpartial = ''
    while input_ != '':
        if input_[0] not in simbolos:
            if verbose:
                out.write('Simbolo no reconocido #%s# en \'%s\'\n' % (input_[0], ginput))
            estado = None
            break
        estado = transiciones[(estado, input_[0])]
        processed += input_[0]
        if estado in finales:
            maxpartial = processed
        input_ = input_[1:]

    if verbose:
        out.write('Maximo match \'%s\' de \'%s\'\n' % (maxpartial, ginput))

    out.write('Resultado final: %r\n' % (estado in finales))