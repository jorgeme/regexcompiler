__author__ = 'jorge'


class Analex:
    def __init__(self, stri):
        self.string = stri

    def peek(self):
        return self.string[0] if len(self.string) > 0 else ''

    def match(self, term):
        if self.peek() == term:
            self.pop()
        elif self.peek() == '':
            raise Exception('Error en la sintaxis, se llego al final de la entrada parseando')
        else:
            raise Exception('Error en la sintaxis, caracter no esperado: "%s" ' % self.peek())

    def pop(self):
        if self.string == '':
            raise Exception('Error en la sintaxis, se llego al final de la entrada parseando')
        else:
            ret = self.peek()
            self.string = self.string[1:]
            return ret

    def empty(self):
        return self.string == ''

