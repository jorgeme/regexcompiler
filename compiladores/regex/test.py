from compiladores.regex import compilar_regex, write_afd

__author__ = 'jorge'

tests = ['a?|b*', '(a.b)*|b', '(a|b)*.a.b.b']
langs = {"a": {"0", "1"}}
symbols = {"b"}
if __name__ == "__main__":
    for rgx in tests:
        print 'analizando "%s"' % rgx
        aristas, inicio, fin, symbols = compilar_regex(rgx, langs, symbols)
        print aristas
        print 'desde %r hasta %r' % (inicio, fin)
        write_afd((symbols, inicio, aristas, fin), 'afds/%s.py' % rgx)








