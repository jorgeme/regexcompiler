__author__ = 'jorge'


def is_consecutive(a, b):
    return ord(a) + 1 == ord(b)


def get_bigger_consecutive_seq(seq):
    lenseq = len(seq)
    if lenseq <= 2:
        return seq
    actual = []
    bigger = []
    for i in range(lenseq - 1):
        actual += seq[i]
        if not is_consecutive(seq[i], seq[i + 1]):
            if len(actual) > len(bigger):
                bigger = actual
            actual = []
    if is_consecutive(seq[lenseq - 2], seq[lenseq - 1]):
        actual += seq[lenseq - 1]
        if len(actual) > len(bigger):
            bigger = actual

    return bigger

