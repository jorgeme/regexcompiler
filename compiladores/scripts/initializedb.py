import os
import sys
import transaction

from sqlalchemy import engine_from_config

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )
from compiladores.models import Lang, Rgx

from ..models import (
    DBSession,
    Base,
    )


def usage(argv):
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri>\n'
          '(example: "%s development.ini")' % (cmd, cmd))
    sys.exit(1)


def main(argv=sys.argv):
    if len(argv) != 2:
        usage(argv)
    config_uri = argv[1]
    setup_logging(config_uri)
    settings = get_appsettings(config_uri)
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    #_SessionClassMethods.close_all()
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
    with transaction.manager:
        minu = Lang('minusculas', set([chr(i) for i in xrange(ord('a'), ord('z') + 1)]))
        mayu = Lang('mayusculas', set([chr(i) for i in xrange(ord('A'), ord('Z') + 1)]))
        letras = Lang('letras', minu.terminos | mayu.terminos)
        numeros = Lang('numeros', set([str(i) for i in range(10)]))
        hexa = Lang('hexadecimal', set(list('0123456789ABCDEFabcdef')))
        alfanumerico = Lang('alfanumerico', letras.terminos | numeros.terminos)
        corchetea = Lang('corchetea', set('['))
        corchetec = Lang('corchetec', set(']'))

        identificador = Rgx('id', 'minusculas.(alfanumerico)*', langs=[minu, alfanumerico])
        array2 = Rgx('array_dim_2', 'minusculas.(alfanumerico)*.corchetea.(0|1).corchetec', langs=[minu,
                                                                                                   alfanumerico,
                                                                                                   corchetea,
                                                                                                   corchetec],
                     simbols={'0', '1'})
        range_test = Rgx('range_test', '(0|1|2|3|4|5|6|7|a|b|c|z).alfanumerico', langs=[alfanumerico])
        wtf = Rgx('wtf', '(minusculas.numeros)|(alfanumerico.numeros)', langs=[minu, alfanumerico, numeros])
        hexbyte = Rgx('hexbyte', '0.x.hexadecimal.hexadecimal', langs=[hexa], simbols={'0', 'x'})

        noerror = Rgx('noerror', 'alfanumerico*', langs=[alfanumerico])

        [DBSession.add(m) for m in [minu,
                                    mayu,
                                    letras,
                                    numeros,
                                    alfanumerico,
                                    hexa,
                                    corchetea,
                                    corchetec,

                                    identificador,
                                    array2,
                                    range_test,
                                    wtf,
                                    hexbyte,
                                    noerror
        ]]

        print 'bd ok'
